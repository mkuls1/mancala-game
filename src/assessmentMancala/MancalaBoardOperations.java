
package assessmentMancala;

/**
 *
 * @author MKuls1
 */
class MancalaBoardOperations 
{
	PitOperations[] pits;
	static final int playingPits = 6, totalPits = 2 * (playingPits + 1);

	//Constructor to initialize pits array with initial value
	MancalaBoardOperations() 
	{
		pits = new PitOperations[totalPits];// 14 pits
		for (int pitNum = 0; pitNum < totalPits; pitNum++)
			pits[pitNum] = new PitOperations();// Adding stones variable to the collectsPit object to form the pits array
	}

	public void initialSetUpForPlay() 
	{
		/*
		 *  Add 6 Stones In each Pits ( 6*6=36 )
		 */
		for (int pitNum = 0; pitNum < totalPits; pitNum++)
			if (!isAMancalaCall(pitNum))
				pits[pitNum].addStones(6);
		/*
		 *  Any Number of Stones can be added in Pits
		 */
	}

	public int stonesInMancala(int playerNum) 
	{
		return pits[getMancala(playerNum)].getStones();
	}

	public int stonesInPit(int playerNum, int pitNum) 
	{
		return pits[getPitNum(playerNum, pitNum)].getStones();
	}

	private int getPitNum(int playerNum, int pitNum) 
	{
		return playerNum * (playingPits + 1) + pitNum;
	}

	private int getMancala(int playerNum) 
	{
		return playerNum * (playingPits + 1);
	}

	private boolean isAMancalaCall(int pitNum) 
	{
		return pitNum % (playingPits + 1) == 0;
	}

	public MancalaBoardOperations makeACopyOfMancalaBoard() 
	{
		MancalaBoardOperations newBoard = new MancalaBoardOperations();
		for (int pitNum = 0; pitNum < totalPits; pitNum++)
			newBoard.pits[pitNum].addStones(this.pits[pitNum].getStones());
		return newBoard;
	}

	/**
	 * @param currentPlayerNum
	 * @param chosenPitNum
	 * @return
	 */
	public boolean doPitsMove(int currentPlayerNum, int chosenPitNum) 
	{
		boolean isPitesGetCalled=false;
		/*
		 * If the last piece Player drop is in an empty cavity same side, 
                Player captures that piece and any pieces in the hole directly opposite to it.
		 */
		int pitNum = getPitNum(currentPlayerNum, chosenPitNum);
		int stones = pits[pitNum].removeStones();
		while (stones != 0) 
		{
			pitNum--;
			if (pitNum < 0)
				pitNum = totalPits - 1;
			if (pitNum != getMancala(otherPlayerNum(currentPlayerNum))) 
			{
				pits[pitNum].addStones(1);
				stones--;
			}
			isPitesGetCalled=true;
		}
		
		if(stones==0 && isPitesGetCalled==false) {
			/*
			 *  Player Selected Pits which have 0 stones in, So Ask player to goAgain and select again Pits, which value is >0;
			 */
			System.out.println("**********************************************************************************************************************************************");
			System.out.println("Warning -> Player [ "+( currentPlayerNum+1)+" ] has selected Pits without any stones in it (Pits=0),So Player has to Go Again and choose Pits from Console.");
			System.out.println("**********************************************************************************************************************************************");
			return true;
		}
		
		if (pitNum == getMancala(currentPlayerNum))
			return true;
		if (whoIsWho(pitNum) == currentPlayerNum && pits[pitNum].getStones() == 1) 
		{
			stones = pits[oppositePitNum(pitNum)].removeStones();
			pits[getMancala(currentPlayerNum)].addStones(stones);
		}
		return false;
	}

	private int whoIsWho(int pitNum) 
	{
		return pitNum / (playingPits + 1);
	}

	private int oppositePitNum(int pitNum) 
	{
		return totalPits - pitNum;
	}

	private int otherPlayerNum(int playerNum) 
	{
		if (playerNum == 0)
			return 1;
		else
			return 0;
	}
	/*
	 * Method to check is game is over : Looping through the no of players pits
	 * If all pits of any one player is 0 the Game is over.Not counting the Big Mancala pit*/
	public boolean isGameOver() 
	{
		for (int player = 0; player < 2; player++) 
		{
			int stones = 0;
			for (int pitNum = 1; pitNum <= playingPits; pitNum++)
				stones += pits[getPitNum(player, pitNum)].getStones();
			if (stones == 0)
				return true;
		}
		return false;
	}

	public void emptyStonesIntoMancalas() 
	{
		for (int player = 0; player < 2; player++)
			for (int pitNum = 0; pitNum <= playingPits; pitNum++) 
			{
				int stones = pits[getPitNum(player, pitNum)].removeStones();
				pits[getMancala(player)].addStones(stones);
			}
	}


}