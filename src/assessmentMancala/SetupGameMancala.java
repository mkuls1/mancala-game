package assessmentMancala;
/**
 *
 * @author MKuls1
 */
 
import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import assessmentMancala.RuleBookMancala;

// My Project


class SetupGameMancala 
{
        /*
         Taking global variables for current Mancala Player, global object to Manage a mancala Board , Player profiles  and a variable
        */
	int currentMancalaPlayer = 0;// a member variable to store current playing user
	MancalaBoardOperations mancalaBoard; // member object variable to store the pits array and variables which do not change value throughout the same
	MancalaPlayersProfile[] playersListByName; // member array variable player information which remains consistent throughout
	int boardReshuffleInArray=0; // member variable to store if the board needs to be reshuffled

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException 
	{

	
        RuleBookMancala.printGameRules(); // Prints Mancala Rules : standard rules from the game.
        System.out.println("Enter Player 1 and Player 2");
        Scanner in = new Scanner(System.in);
        String playerOne= in.next();
        String playerTwo = in.next();
        
        System.out.println("Enter Y or N");
        String reschuffleVar= in.next();
       
		SetupGameMancala playGameMancala = new SetupGameMancala(playerOne,playerTwo,reschuffleVar); // Passing the two player Names and Board reschuffle value
		playGameMancala.playMancala();
	}

	/**
	 * @param firstPlayerName
	 * @param secondPlayerName
	 */
	SetupGameMancala(String firstPlayerName, String secondPlayerName,String boardReshuffle) 
	{

		mancalaBoard = new MancalaBoardOperations();
		mancalaBoard.initialSetUpForPlay();
		playersListByName = new MancalaPlayersProfile[2];// creating the array for Player Profiles

		playersListByName[0] = new MancalaPlayersProfile(firstPlayerName, 0);
		playersListByName[1] = new MancalaPlayersProfile(secondPlayerName, 1);
		currentMancalaPlayer = 0;

            
            //Reshuffle will change user Pits to predefined Pits.
           

		if(boardReshuffle=="Y" || boardReshuffle=="y") {
			boardReshuffleInArray=1;
		}else {
			boardReshuffleInArray=0;
		}
	}

	/**
	 * @throws IOException
         * 
         * This method starts the game-play of Mancala.
	 */
	public void playMancala() throws IOException 
	{

		displayMancalaBoard();// To display original board each time
		while (!mancalaBoard.isGameOver()) 
		{
			String playerNamefromArray;
			if(currentMancalaPlayer==0)
			{
				playerNamefromArray=playersListByName[0].getName();	
			}else {
				playerNamefromArray=playersListByName[1].getName();	
			}

			/*
			 *  Calling Function: selectTheMove to Determine the Move, 
                        Ask for Input at main Console.
			 */
			int pitNum=0;
			int sufflePitNumber = playersListByName[currentMancalaPlayer].selectTheMove(mancalaBoard); 
			if(boardReshuffleInArray==1) {
				pitNum=doBoardReshuffle(sufflePitNumber,currentMancalaPlayer);
			}else {
				pitNum=sufflePitNumber;
			}
			
				/*
				 *  Display Over Console, which player moved was along with Changed Mancala Board
				 */

			 System.out.println("Player [ " + playerNamefromArray + " ] moved from " + sufflePitNumber);

			// the move:-->  User has Selected the Pits which need to be moved , which are stored in variable pitNum. 

			/*
			 *  If user runs into  own cavity, deposit one stone in it. If user runs into your opponent's cavity skip it.
			 *  If the last piece  dropped is in my own cavity,  get a free turn.
			 */
			boolean checkgoAgainCall = mancalaBoard.doPitsMove(currentMancalaPlayer, pitNum);

			/*
			 *  Display Over Console, which player moved was along with Changed Mancala Board
			 */

			displayMancalaBoard();

			if (!checkgoAgainCall) 
			{	
				// If the current player does not go again,switch to the other player
				if (currentMancalaPlayer == 0) 	
					currentMancalaPlayer = 1;
				else
					currentMancalaPlayer = 0;
			}else
			{
				//If the last piece you drop is in your own Mancala , User get a free turn.
				System.out.println("Player [ " + playerNamefromArray + " ] goes again");
			}
		}
		/*
		 * The game ends when all six spaces on one side of the Mancala board are empty.
		 */

		// Game is over , mancalaBoard empty stones,
		mancalaBoard.emptyStonesIntoMancalas(); 

		// Display final mancalaBoard. 
		displayMancalaBoard(); 			


		/*
		 * Below section determined Who is Winner And Winner Award goes to......
		 */
		if (mancalaBoard.stonesInMancala(0) > mancalaBoard.stonesInMancala(1)){
			System.out.println("************* Congratulations..! You Won this Game ******************************");
			System.out.println(playersListByName[0].getName() + " wins");
			System.out.println("*******************************************");
		}
		else if (mancalaBoard.stonesInMancala(0) < mancalaBoard.stonesInMancala(1)) {
			System.out.println("************* Congratulations..! You Won this Game ******************************");
			System.out.println(playersListByName[1].getName() + " wins");
			System.out.println("*******************************************");
		}
		else {
			System.out.println("************* If the score of a game is equal, then the game is a tie :) ******************************");
			System.out.println("Tie");// If this possible for safe side :)
			System.out.println("*******************************************");
		}

	}

	/**
	 * @param userInputasPit
	 * @param currentPlayer
	 * @return
	 */
	public int doBoardReshuffle(int userInputasPit,int currentPlayer)
	{
		int newPits = 0;
		if(currentPlayer==0) {
			if(userInputasPit==6)
				newPits= 1;
			else if(userInputasPit==5)
				newPits= 2;
			else if (userInputasPit==4)
				newPits= 3;
			else if (userInputasPit==3)
				newPits= 4;
			else if (userInputasPit==2)
				newPits= 5;
			else if (userInputasPit==1)
				newPits= 6;
		}else {
			newPits=userInputasPit;
		}
		return newPits;

	}
	/**
	 * 
	 */

	private void displayMancalaBoard() 
	{

		/*
		 *  Function : displayMancalsBoardConsole
		 *  This function is called n times, till the game is not finished.
		 *  step 1 : 	Display Player 2 Pits and Player 2 Information
		 *  Step 2 :  	Display Mancala information Both side
		 *  Step 3 : 	Display Player 1 Pits and Player 1 Information
		 *  Step 4 :	print Seperater ( ******* )
		 */

		try {
			/*
			 * Sleep method here since user needs time to see and remember the move or Pits positions
			 */
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("\n");
		String separator = ""; 
		System.out.println("+++++++++++++++++++++++++++++++++++++");
		System.out.print("      ");

		//Step 1 : 	Display Player 2 Pits and Player 2 Information
		for (int i = 1; i <= MancalaBoardOperations.playingPits; i++) 
		{
			System.out.print(mancalaBoard.stonesInPit(1, i) + "    ");
			separator += "     ";// adding space fillers so that later the no of spaces count towards the mancala pit
		}

		//	Step 1.1 : 	Player 2 Information
		displayPlayer(1); 

		//	Step 2 :  	Display Mancala information Both side. Each player has a store (called a Mancala) to the right side of the Mancala board.

		System.out.print(mancalaBoard.stonesInMancala(1) + "    "); 
		System.out.print(separator);
		System.out.println(mancalaBoard.stonesInMancala(0));

		System.out.print("      ");

		//	Step 3 : 	Display Player 1 Pits and Player 1 Information
		for (int i = MancalaBoardOperations.playingPits; i >= 1; i--)
			System.out.print(mancalaBoard.stonesInPit(0, i) + "    "); 

		//	Player 3.1 Information
		displayPlayer(0); 
		System.out.println("*************************************");
	}

	/**
	 * @param mancalaPlayerNum
	 */
	private void displayPlayer(int mancalaPlayerNum) 
	{
		/* 
		 *  In a Console User turn need to be printed which will indicate ,Who is Who and who is playing.
		 *  Below section is buld to determine the same
		 */

		// Check If it this player's turn,
		if (currentMancalaPlayer == mancalaPlayerNum){  
			System.out.print("            -->> ");        // --> Sign mean User Turn, Active user, Same can be visble in Console screen
		}else {
			System.out.print("                 ");       // User is not active , user loose his/her turn, Same can be visble in Console screen
		}

		/*
		 *  Below section will added to show User/Player Information to Main Console eg.. user name or User Number
		 */
		int playerCounter;
		if(mancalaPlayerNum==0) {
			playerCounter=1;
		}else {
			playerCounter=2;
		}

		System.out.println("Player " + playerCounter + " ( " + playersListByName[mancalaPlayerNum].getName() + ")");
	}


}









