
package assessmentMancala;

/**
 *
 * @author MKuls1
 */

class PitOperations 
{
	int stones;
	
	//Constructor initializes stones to 0.
	PitOperations() 
	{
		this.stones = 0;
	}
	
	// Returns the no of stones
	public int getStones() 
	{
		return stones;
	}
	/*
	 * Increments the stones value*/
	public void addStones(int stones) 
	{
		this.stones += stones;
	}
	/*
	 * This method has a local variable which stores value of stones and after the stones is set to 0,it returns
	 * the stones value in the pit.*/  
	
	public int removeStones() {
		int stonesloc = this.stones;
		this.stones = 0;
		return stonesloc;
	}

	
}