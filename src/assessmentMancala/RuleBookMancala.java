/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assessmentMancala;

/**
 *
 * @author MKuls1
 */
public class RuleBookMancala
{
	
	public static void printGameRules()
	{
                String newLine = System.getProperty("line.separator");
                
                   System.out.println(" Mancala is one of the oldest known games to still be widely played today."
                        +newLine +" Mancala is a generic name for a family of two-player turn-based strategy board games played with small stones, beans, "
                        + newLine +" or seeds and rows of holes or pits in the earth, a board or other playing surface. "
                        + newLine +" The objective is usually to capture all or some set of the opponent's pieces. ");
		System.out.println("\t\t*************************************");
		System.out.println("\t\t   WELCOME TO MANCALA GAME!    ");
		System.out.println("\t\t*************************************");
		System.out.println(".........Rules of the GAME.....");
		System.out.println("\t1 : Minimum Pits Which you can select is 1 and Maximum is 6. ");
		System.out.println("\t2 : Every Player has to play this game anticlockwise.");
		System.out.println("\t\t2.1 : For Player 2 : Right to Left ");
		System.out.println("\t\t2.2 : For Player 1 : Left to Right ");

		System.out.println("\t3 : In a display console, Pits which called ( Mancala ) shown as '0',THE BIG PIT");
		System.out.println("\t4 : Input should only be numeric, not symbols or characters ");
		System.out.println("\t5 : There is a delay of 1 second between each move.");
		System.out.println("\t6 : Updated Board will be printed after each move");
		System.out.println("   ");
		System.out.println("Lets play Mancala");
		System.out.println("___________________________________________________________________");
		
    }

	
}