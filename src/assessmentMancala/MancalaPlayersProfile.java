
package assessmentMancala;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author MKuls1
 */
class MancalaPlayersProfile 
{
	String isARealPlayername;
	int mancalaPlayerNum;
	MancalaPlayersProfile(String name, int playerNum) 
	{
		this.isARealPlayername = name;
		this.mancalaPlayerNum = playerNum;
	}

	public String getName() 
	{
		if (isARealPlayername != null)
			return isARealPlayername;
		else
			return "AI";
	}

	public int selectTheMove(MancalaBoardOperations board) throws IOException 
	{

		int pitNum = 0;
		if (isARealPlayername != null) 
		{ 
			/*
			 *  The below if will check : If Human is playing or Not, isARealPlayername!=null then , ask User to provide Input else calculate Move.
			 */
			try {


				System.out.println(" ______________________________________________________________________________");
				System.out.println(" Last/Recent Updated Board has been printed as above along with last user turn.");
				System.out.println(" ______________________________________________________________________________");
				System.out.println(" ---Current Player is---\n");
				Scanner scanner = new Scanner(System.in);
				System.out.print("<<<<<"+ isARealPlayername + ">>>>>  PLEASE ENTER A PIT TO MOVE FROM :->   "); // Prompt User Input need to be provided
				scanner:
					while(scanner.hasNext()) {
						if(scanner.hasNextInt()){
							pitNum = scanner.nextInt();
							if((pitNum<=6 && pitNum>=1))  {
								break scanner;
							} else {
								System.out.println("******************************************************************************");
								System.out.println("PLAYER HAS ENTERED A WRONG PIT... YOUR SELECTION SHOULD BE IN BETWEEN ( 1 TO 6 ), Please Provide correct Input Below");
								System.out.println("******************************************************************************");
								System.out.print("<<<<<"+ isARealPlayername + ">>>>>  PLEASE ENTER A PIT TO MOVE FROM :->   "); // Prompt User Input need to be provided
							}
						} else {
							System.out.println("******************************************************************************");
							System.out.println("ERROR: Invalid Input... YOUR SELECTION SHOULD BE IN BETWEEN ( 1 TO 6 ) and Numeric, Information is not in the correct format");
							System.out.println("******************************************************************************");
							System.out.print("<<<<<"+ isARealPlayername + ">>>>>  PLEASE ENTER A PIT TO MOVE FROM :->   "); // Prompt User Input need to be provided
							scanner.next();
						}
					}

			} catch (NumberFormatException ex) {
				System.err.println("Could not convert input string " + ex.getMessage()+"\n");
			}	
			return pitNum; 
		}else {
                    
                    System.out.println("Invalid Player, AI is not playing");
                    return pitNum;

		}
	}


}